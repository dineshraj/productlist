import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UrlList } from '../common/UrlList';
import { Observable } from 'rxjs';

interface ProductList {
  id: number;
  name: string;
  state: string;
  zip: number;
  amount: number;
  qty: number;
  item: string;

}

@Injectable({
  providedIn: 'root'
})
export class ProductService {


  constructor(private httpClient: HttpClient) { }

  fetchProductList() : Observable<ProductList[]>{
    let params: HttpParams = new HttpParams()
    let  jsonUrl = UrlList.FetchProductlist;
    return this.httpClient.get<ProductList[]>(jsonUrl,{params});
  }

  fetchProductListTitle(){

    let params: HttpParams = new HttpParams()
    let  jsonUrl = UrlList.FetchProductListTitle;
    return this.httpClient.get(jsonUrl,{params});

  }

  DeleteProdutItem(idx: any){
    let jsonUrl = UrlList.DeleteProdutItem;
    let params: HttpParams = new HttpParams()
    .set("id", idx)
    return this.httpClient.get(jsonUrl,{params});
  }

  UpdateProductList(me, jsonData: any){

    let fd: FormData = new FormData();
    fd.append("newList", jsonData);

    var xhr = new XMLHttpRequest();
    xhr.open('POST', UrlList.UpdateProductList , true);
    xhr.onload = function () {
      me.onSuccessUpdate();
    };
    xhr.send(fd);

  }
}
