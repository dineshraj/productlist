import { HttpClient } from "@angular/common/http";
import { Component } from "@angular/core";
import { of } from "rxjs";
import { ProductService } from "./product.service";
import {ListViewComponent} from "../app/list-view/list-view.component";
import { request } from "http";

describe('Product Service' ,()=>{

    let httpClientSpy : jasmine.SpyObj<HttpClient>

    let productService : ProductService;
    let component : ListViewComponent;
    let mockPostService : any ;

    let resultStatus : string = 'updated';

    let POST=[

        {
            id : 1,
            name : 'Liquid Saffron',
            state : 'NY',
            zip : 08998,
            amount : 25.43,
            qty : 7,
            item : 'XCD45300'
        },
        {
            id : 2,
            name : 'Mostly Slugs',
            state : 'PA',
            zip : 19008,
            amount : 13.30,
            qty : 2,
            item : 'AAH6748'
        }
    ];

    beforeEach(()=>{
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']) 
        productService = new ProductService(httpClientSpy);

        mockPostService = jasmine.createSpyObj(['fetchProductList', 'delete']);
        component = new ListViewComponent(mockPostService);
    } )


    describe('fetchProductList',()=>{

        it('Should return product List',(done : DoneFn)=>{
            httpClientSpy.get.and.returnValue(of(POST));
            productService.fetchProductList().subscribe({
                next :(posts)=>{
                    expect(posts).toEqual(POST);
                    done();
                },
                error : ()=>{
                    done.fail;
                }
            })

        }

        );
        expect(httpClientSpy.get).toHaveBeenCalledTimes(1);

    });

    
    describe('DeleteProdutItem',()=>{

        it('Should delete product List',(done : DoneFn)=>{
            component.ProductList=POST;
            productService.DeleteProdutItem(POST[1]);
            expect(component.ProductList.length).toBe(1);

        }

        );
    });

    describe('UpdateProductList',()=>{

        it('Should return success status',(done : DoneFn)=>{
            httpClientSpy.get.and.returnValue(of(POST));
            productService.UpdateProductList(component,POST);
            expect(resultStatus).toEqual('updated');

        }

        );

    });

    describe('deleteRow',()=>{

        it('Should delete a row List',(done : DoneFn)=>{
            component.ProductList=POST;
            component.deleteRow();
            expect(component.ProductList.length).toBe(1);

        }
        );
    });



}

)