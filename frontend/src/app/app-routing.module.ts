import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductListComponent } from '../app/product-list/product-list.component';
import { ListViewComponent } from './list-view/list-view.component';


const routes: Routes = [
  { path: 'list', component: ProductListComponent, pathMatch: 'full'},

];


@NgModule({
  imports: [RouterModule.forRoot(routes,{ useHash : true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
