import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  @ViewChild('listView') listView;

  
  constructor() { }

  ngOnInit(): void {
  }


  deleteRow(){

    this.listView.deleteRow();

  }

  addRow(){

    this.listView.addRow();


  }

  save(){

    this.listView.update();
    
  }

}
