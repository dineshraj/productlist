import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ProductService } from 'src/services/product.service';
import { InputDataComponent } from '../input-data/input-data.component';
import { InputData } from '../input-data/inputData';
import { Common} from '../../common/Common';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.css'],
})
export class ListViewComponent implements OnInit {

  @ViewChild('inputDlg') inputDialog;
  inputdataParams: InputData = new InputData();
  
  ProductList: any =[];
  ProductListCols : any =[];
  selectedProducts : any =[];


  constructor(private productService : ProductService) { 

    // Columns initialized
    this.ProductListCols = [
      { field: 'id', header: 'ID' },
      { field: 'name', header: 'NAME' },    
      { field: 'state', header: 'STATE' },  
      { field: 'zip', header: 'Zip' }, 
      { field: 'amount', header: 'AMOUNT' },
      { field: 'qty', header: 'QTY' },
      { field: 'item', header: 'ITEM' }

  ];

  }


  ngOnInit(): void {

    this.fetchProductList();
    //this.fetchProductListTitle();

  }

  fetchProductListTitle(){

    // Fetching title from data file deprecated
    var me=this;
    this.productService.fetchProductListTitle().subscribe((data: any) =>{

      if(data){
        let temp=data;

        for(let n = 0 ; n < data.length; n++){
          me.ProductListCols.push({ field:data[n] , header : data[n].toUpperCase() });
        }

        me.fetchProductList();


      }


     },
       err => {
           console.log(err)},
       () => {
           console.log('id-returned');
       }); 

  }


  fetchProductList(){

    // Fetch List of Products form data file
        var me=this;
       this.productService.fetchProductList().subscribe((data: any) =>{

        if(data){
          this.ProductList=data;  
        }
        },
          err => {
              console.log(err)},
          () => {
              console.log('id-returned');
          }); 

  }


  // Show dialog to Add product list
  addRow(){

    this.inputdataParams = new InputData();
    this.inputdataParams.id = this.getNextID();

    this.inputDialog.show(this.inputdataParams);


  }

// Add new item in the list confirmed
  newProductAdded(itm){

    const data = itm as InputData;
    
    const newItem = ({id: this.getNextID(),
         name: data.name, state: data.state,
         zip: data.zip, amount: data.amount,
         qty:data.qty, item : data.item,
    });

     this.ProductList = [...this.ProductList, newItem];

  }

// reload table after updated to File
  onSuccessUpdate(data){
    this.fetchProductList();


}

    // Deleting productList items
  deleteRow(){

    var me=this;

    const dellist = this.ProductList;

    this.selectedProducts.forEach(element => {

      me.ProductList.forEach( function (el,index) {
          if (parseInt(element.id) == parseInt(el.id)) {
            dellist.splice(index, 1);
          }
      });
     
    });

    dellist.forEach(function (element,idx) {
        element.id = (idx + 1).toString();
    });

    this.ProductList = [...dellist];

    this.selectedProducts=[];

    }

    // Generating id for next item 
    getNextID(): number {
      let maxID = 0;
      if (this.ProductList.length > 0) maxID = Math.max.apply(Math,  this.ProductList.map(function(a){
          const val: number = Number.parseInt(a.id);
          return Number.isNaN(val) ? 0 : val;
      }));

      return maxID + 1;
  }


  // Updating the modified product list 
  update(){


    // Saving Modifications  back to data file
    const JsonData=JSON.parse(JSON.stringify(this.ProductList));

    var titleObj= [];

    for(let i=0 ; i< this.ProductListCols.length ; i++) {
      titleObj.push(this.ProductListCols[i].field);

    }

    JsonData.splice(0, 0, titleObj);

    let jsonData = JSON.stringify(JsonData);   
    this.productService.UpdateProductList(this,jsonData);

  }

  validateName(evt, data){

    Common.validateName(evt, data);

  }

  validateItem(evt, data){

    Common.validateItem(evt, data);

  }

  isValidZipCode(evt, data){

    Common.isValidZipCode(evt, data);

  }

  restrictDecimal(evt, data){

    Common.restrictDecimal(evt, data);

  }
     

}
