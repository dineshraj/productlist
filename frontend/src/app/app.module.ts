import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ListViewComponent } from './list-view/list-view.component';

import {TableModule} from 'primeng/table';
import {ToastModule} from 'primeng/toast';
import { ButtonModule } from 'primeng/button';
import { InputText} from 'primeng/inputtext';
import { HttpClientModule } from '@angular/common/http';
import { DialogModule } from 'primeng/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



import { ProductService } from '../services/product.service';


import { MessagesModule } from 'primeng/messages';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import { MessageService } from 'primeng/api';
import { FormsModule } from '@angular/forms';
import { InputDataComponent } from './input-data/input-data.component';




@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    ListViewComponent,
    InputDataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TableModule,
    ToastModule,
    ButtonModule,
    HttpClientModule,
    MessagesModule,
    ConfirmDialogModule,
    FormsModule,
    DialogModule,
    BrowserAnimationsModule

  ],
  providers: [MessageService, ConfirmationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
