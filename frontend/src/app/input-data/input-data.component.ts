import { Component, OnInit, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { InputData } from './inputData';
import { Common} from '../../common/Common';

@Component({
  selector: 'app-input-data',
  templateUrl: './input-data.component.html',
  styleUrls: ['./input-data.component.css'],
  inputs : ['ProductList']

})
export class InputDataComponent implements OnInit {

  @Output() notifyNewItemAdded: EventEmitter<any> = new EventEmitter<any>();


  inputdataParams: InputData;
  display : boolean = false;
  _ProductList : any =[];

  constructor() { }

  ngOnInit(): void {
    this.inputdataParams = new InputData();

  }

  get ProductList():any{
    return this._ProductList;
  }

  set ProductList(data: any) {
      this._ProductList = data;
  }

  show(data){

    Object.assign(this.inputdataParams, data);

    this.display=true;

  }

  onClickAddItem(){

    var isValid=Common.validateItem(this.inputdataParams,this._ProductList);

    if(isValid){
      this.notifyNewItemAdded.emit(this.inputdataParams);
      this.display=false;
    }
    else{
      alert("Same Item exsits , please check the list");
    }

  }

  validateName(evt, data){

    Common.validateName(evt, data);

  }

  validateItem(evt, data){

    Common.validateItem(evt, data);

  }

  isValidZipCode(evt, data){

    Common.isValidZipCode(evt, data);

  }

  restrictDecimal(evt, data){

    Common.restrictDecimal(evt, data);

  }


}
