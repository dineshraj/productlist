export class InputData{

    id : number = 1;
    name : string ='';
    state : string =''
    zip : number ;
    amount : number = 0;
    qty : number = 1 ;
    item : string ='';

}