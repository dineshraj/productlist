export class UrlList {

    static RootUrl: string = " http://localhost:8080/SampleProject/ExcelTableApp/ProductList/application-test/backend/";


    static FetchProductlist = UrlList.RootUrl + 'fetchProductList.php';
    static FetchProductListTitle= UrlList.RootUrl + 'fetchProductListTitle.php';

    
    static AddNewProdutItem = UrlList.RootUrl + 'addNewProdutItem.php';
    static UpdateProductList = UrlList.RootUrl + 'updateProductList.php';
    static DeleteProdutItem= UrlList.RootUrl + 'deleteProdutItem.php';

}