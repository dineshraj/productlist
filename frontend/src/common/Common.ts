import {Component, Injectable, Input, Output, EventEmitter} from '@angular/core';


export class Common {
    
  public static validateItem(inputdataParams, ProductList){

    for( let i=0; i< ProductList.length ; i++){
      if(ProductList[i].item==inputdataParams.item){
        return false;
      }

    }
    return true;

  }


  // Restricting Decimal value for zip ,quantity
  public static restrictDecimal(event, input){
    if(event.key==='.'){event.preventDefault();}
  }

  // validating postal Zip code
  public static isValidZipCode(e, zip) 
  {
  
    var zipcodeValue = zip.toString() + String.fromCharCode(e.keyCode);;
    if (zipcodeValue.length<=5) { return true;}
    var regex = new RegExp(/^[0-9]{4}[a-z]{2}$/i);
    if (zipcodeValue.length==5) {
        regex = new RegExp(/^[0-9]{4}[a-z]{1}$/i);
    }
    if (regex.test(zipcodeValue) != true) {   
        return false;        
    }

  }

  public static validateName(e,name){

    const nameRegex = /^[A-Za-z]+$/;

    const isValidName = nameRegex.test(name);
    if (isValidName) {
      return true;
    } else {
      e.preventDefault();
    }

  }
}


