<?php

use PHPUnit\Framework\TestCase;

class ProductListTest extends TestCase{

    // Test case  for updating   product list
    public function testUpdateList(){
        
        $productLsit = new ProductList ;

        $data = '[{"id":"1","name":"Liquid Saffron","state":"NY","zip":"08998","amount":"25.43","qty":"7","item":"XCD45300"},{"id":"2","name":"Mostly Slugs","state":"PA","zip":"19008","amount":"13.30","qty":"2","item":"AAH6748"},{"id":"3","name":"Jump Stain","state":"CA","zip":"99388","amount":"56.00","qty":"3","item":"MKII4400"},{"id":"4","name":"Scheckled Sherlock","state":"WA","zip":"88990","amount":"987.56","qty":"1","item":"TR909"}]';

        $result = $productLsit-> updateProductList($data);

        $this->assertEquals('updated');

    }

    // Test case  for updating  invalid product list
    public function updatenonvaildCasetest(){
        
        $productLsit = new ProductList ;

        $data = '[{"id":"1","name":"Liquid Saffron","state":"NY","zip":"089542598","amount":"25.4g3","qty":"1s","item":"XCD45300"},{"id":"2","name":"Mostly Slugs","state":"PA","zip":"19008","amount":"13.30","qty":"2","item":"AAH6748"},{"id":"3","name":"Jump Stain","state":"CA","zip":"99388","amount":"56.00","qty":"3","item":"MKII4400"},{"id":"4","name":"Scheckled Sherlock","state":"WA","zip":"88990","amount":"987.56","qty":"1","item":"TR909"}]';

        $result = $productLsit-> updateProductList($data);

        $this->assertEquals('failed');

    }

    // check for valid Field with the product list
    public function validateEachField(){
        
        $productLsit = new ProductList ;

        $data = array(array("id"=>"1","name"=>"Liquid Saffron","state"=>"NY","zip"=>"089542598","amount"=>"25.43","qty"=>"7","item"=>"XCD45300"),array("id"=>"2","name"=>"Mostly Slugs","state"=>"PA","zip"=>"19008","amount"=>"13.30","qty"=>"2","item"=>"AAH6748"),array("id"=>"3","name"=>"Jump Stain","state"=>"CA","zip"=>"99388","amount"=>"56.00","qty"=>"3","item"=>"MKII4400"),array("id"=>"4","name"=>"Scheckled Sherlock","state"=>"WA","zip"=>"88990","amount"=>"987.56","qty"=>"1","item"=>"TR909"));

        $result = $productLsit-> validateEachField($data);

        $this->assertEquals(true);

    }

 
    

    // deleting product item with id
    public function deleteproductItem(){
    
        $productLsit = new ProductList ;

        $data = [];
        $data['id']='1';

        $result = $productLsit-> deleteProdutItem($data);

        $this->assertEquals("Deleted Successfully");

    }
   

    // check for fetching title from data csv file
    public function fetchProductListTitle(){
        
        $ProductTitles = new ProductTitles ;

        $data = array("id","name","state","zip","amount","qty","item"=>"XCD45300");

        $result = $ProductTitles-> fetchProductListTitle();

        $this->assertEquals($data);

    }

    // check for fetching data from  csv file
    public function getFileContent(){
    
        $Common = new Common ;

        $data = array(array("id"=>"1","name"=>"Liquid Saffron","state"=>"NY","zip"=>"089542598","amount"=>"25.43","qty"=>"7","item"=>"XCD45300"),array("id"=>"2","name"=>"Mostly Slugs","state"=>"PA","zip"=>"19008","amount"=>"13.30","qty"=>"2","item"=>"AAH6748"),array("id"=>"3","name"=>"Jump Stain","state"=>"CA","zip"=>"99388","amount"=>"56.00","qty"=>"3","item"=>"MKII4400"),array("id"=>"4","name"=>"Scheckled Sherlock","state"=>"WA","zip"=>"88990","amount"=>"987.56","qty"=>"1","item"=>"TR909"));

        $result = $Common-> getFileContent();

        $this->assertEquals($data);

    }


}

?>