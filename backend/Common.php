<?php

trait Common{

    function getFileContent(){

        $list=[];

        $fileName ="data.csv";  

        try{

            if(file_exists($fileName)){  // check if file exists

                $file = fopen($fileName, "r");
    
                while(! feof($file)){
                    $arr=fgetcsv($file);
                    array_push($list, $arr);
                }
        
                fclose($file);
            }
            else{

                // Creating csv File with Headers if file not found
                createEmptyCSVFile($fileName);
            }       

        }
        catch (Exception $e){
            throw new Exception( 'Unable to Load file',0,$e);
         }

         return $list;
    }

    
    function organizeData($list){

        $organizedList =[];

        $title=array_splice($list,0,1);

        if(count($title)!=1 )  return [];

        for($i=0;  $i<count($list) ; $i++ ){

            $row=[];

            for($j=0;  $j<count($title[0]) ; $j++ ){
                $temp=$title[0][$j];
                $row[$temp]=$list[$i][$j];        
            }

            $organizedList[]=$row;

        }


       return $organizedList;

    }

    function GetDocHeaders($list){

        $TitleList =[];

        $title=array_splice($list,0,1);

        if(count($title)!=1 )  return [];

            $TitleList=[];

            for($j=0;  $j<count($title[0]) ; $j++ ){
                $temp=$title[0][$j];
                $TitleList[]=$temp;        
            }

       return $TitleList;

    }


    function createEmptyCSVFile($fileName){
        $contents = array('ID','NAME','STATE','ITEM','ZIP','AMOUNT','QTY');

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='+$fileName);

        $file = fopen("php://output", "w");

        foreach($contents as $content){
            fputcsv($file,explode(',',$content));
        }
        fclose($file);
    }

}



?>