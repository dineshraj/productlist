<?php
include_once('Common.php');
include_once('OrganizeDatasets.php');

class ProductList implements OrganizeDatasets{

    use Common;

    // Product List fetched from data csv file
    function fetchProductList(){


        $list=[];

        $getFileContent = new ProductList();
        $list= $getFileContent->getFileContent();

        $organizedData = new ProductList();
        $organizedDataSet= $organizedData->organizeData($list);

        return  $organizedDataSet;
        
    }  

    // Delete product list item
    function deleteProdutItem($id){

        $result="Deletion Failed";
           
            $id=(int)$id+1; // SInce we are passing only element without title

            $file_handle = fopen("data.csv", "rw");
        
            while (!feof($file_handle) ) {
                $line_of_text = fgetcsv($file_handle, 1024);    
                if ($id != $line_of_text[0]) {
                    fputcsv($file_handle, $line_of_text);

                }
                $result="Successfully Deleted";

            }
            fclose($file_handle);
        
        return $result;
        
    }

    //  update csv file with modified  data
    function updateProductList($list){

        $status =[];

        $row =json_decode($list,true);
        
        $fp = fopen('data.csv', 'w');

        $result= validateEachField($row);
        if(!$result) return 'Failed';
        
        foreach ($row as $fields) {
            
            fputcsv($fp, $fields);
        }
        
        fclose($fp);

        $status= 'updated';

        return $status;

    }

    function validateEachField($list){

        foreach($list as $x => $val) {

            if($x=='id'){
                if(!is_numeric($val)) {
                    return false ;
                }
           
            }
            else if($x=='name'){
                if (! preg_match ("/^[a-zA-z]*$/", $val) ) {
                    return false ;
                }           
            }
            else if($x=='zip'){
                if (! preg_match ("/^[0-9]{4}[a-z]{1}$/i", $val) ) {
                    return false ;
                }           
            }
            else if($x=='amount'){
                if (! is_float ($val) ) {
                    return false ;
                }           
            }
            else if($x=='qty'){
                if (! is_numeric ($val) ) {
                    return false ;
                }           
            }
          }

          return true;
            
        

    }

}


?>