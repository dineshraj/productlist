# GreenIT Application Challenge

This project is will have the product list table fetched from csv file and can be added or modified.
those modifications saved back to the data csv file 

Objective: This test will be a fully functional web application that will display the given data from a csv file, and allow full editing capabilties of the data.

## Application Version Requirements
1. Front end with angular 10+ version
2. php with 7.2+
3. front end build with npm
4. php  as raw files

## Backend Requirements
1. The backend will provide data to the frontend through a RESTful API
2. The API will allow GET requests for getting all records. 
3. The API will allow POST requests for save, update, and delete.  
4. The API response will be JSON formatted data.
5. The data will be read from a provided data file.
6.  Modified data will be udpated with with udpateproductlist API
7.  product list validated before udpated.

## Frontend Requirements
1. Data will be displayed in a table view.
2. Each column header in the table will correspond to a field name in the data file.
3. Each row can be edited with every field 
4. Record Creation done with input modal , using parent child communication in front end
5. click on save to apply changes in CSAVile
6. Records fetched form csv file using fetch productlist service with observables and interface
7. Every new added item validated before  clicking on add button

  
## Data
1. Data will be provided as csv text file.
2. The fist row of the data file will contain the field names.


	
